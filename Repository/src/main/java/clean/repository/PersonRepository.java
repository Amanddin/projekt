package clean.repository;

import clean.domain.Person;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    public Optional<Person> findByUuid(String uuid);
}

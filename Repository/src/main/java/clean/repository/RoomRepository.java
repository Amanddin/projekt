package clean.repository;

import clean.domain.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {
}

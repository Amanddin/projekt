package clean.repository;

import clean.domain.Cleaning;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thomas
 */
@Repository
public interface CleaningRepository extends JpaRepository<Cleaning, Long> {

    public List<Cleaning> findByRoomId(Long roomId);

    public List<Cleaning> findByCleanerIdAndExecutionDateTimeGreaterThan(
            Long cleanerId,
            LocalDateTime executionDateTime);
}

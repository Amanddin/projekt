package clean.domain;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.*;

/**
 *
 * @author thomas
 */
@Entity
public class Cleaning implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime executionDateTime;
    @ManyToOne
    private Person cleaner;
    @ManyToOne
    private Room room;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getExecutionDateTime() {
        return executionDateTime;
    }

    public void setExecutionDateTime(LocalDateTime executionDateTime) {
        this.executionDateTime = executionDateTime;
    }

    public Person getCleaner() {
        return cleaner;
    }

    public void setCleaner(Person cleaner) {
        this.cleaner = cleaner;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
    
    public Integer getCleaningFee() {
        return getCleaner().getFeePerCleaning();
    }
}

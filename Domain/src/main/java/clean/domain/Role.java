package clean.domain;

/**
 *
 * @author thomas
 */
public enum Role {
    ADMIN, USER;
}

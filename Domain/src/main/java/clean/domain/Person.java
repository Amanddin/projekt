package clean.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.UUID;
import javax.persistence.*;

/**
 *
 * @author thomas
 */
@Entity
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private LocalDate lastSalaryDeadline;
    private Integer feePerCleaning;
    @Column(nullable = false, unique = true)
    private String uuid;
    @Enumerated(EnumType.STRING)
    private Role role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getLastSalaryDeadline() {
        return lastSalaryDeadline;
    }

    public void setLastSalaryDeadline(LocalDate lastSalaryDeadline) {
        this.lastSalaryDeadline = lastSalaryDeadline;
    }

    public Integer getFeePerCleaning() {
        return feePerCleaning;
    }

    public void setFeePerCleaning(Integer feePerCleaning) {
        this.feePerCleaning = feePerCleaning;
    }

    public String getUuid() {
        return uuid;
    }

    public String createUuid() {
        return uuid = UUID.randomUUID().toString();
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}

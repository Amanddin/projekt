package clean.domain;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author thomas
 */
public class PersonTest {

    private final Person testObject = new Person();

    @Test
    public void uuidCreationShouldBeValid() {
        assertTrue(testObject.createUuid().matches("^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$"));
    }

    @Test
    public void createdTestIdShouldBeConsistent() {
        String uuid = testObject.createUuid();
        assertEquals(uuid, testObject.getUuid());
    }

    @Test
    public void recreatedTestIdShouldBeNewValue() {
        String uuid = testObject.createUuid();
        assertNotEquals(uuid, testObject.createUuid());
    }
}

package clean.business.domain;

import clean.domain.Cleaning;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author thomas
 */
public class SalaryReportTest {
    
    public SalaryReportTest() {
    }

    @Test
    public void sumOfCleaningFeesShouldMatch() {
        List<Cleaning> cleanings = List.of(
                cMock(45),
                cMock(20),
                cMock(20),
                cMock(33));
        var testObject = new SalaryReport(null, cleanings);
        assertEquals(118, testObject.getAccumulatedSalary());
    }
    
    private Cleaning cMock(Integer fee) {
        var result = mock(Cleaning.class);
        when(result.getCleaningFee()).thenReturn(fee);
        return result;
    }
}

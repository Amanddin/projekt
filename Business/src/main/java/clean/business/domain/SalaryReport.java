package clean.business.domain;

import clean.domain.*;
import java.util.List;

/**
 *
 * @author thomas
 */
public class SalaryReport {

    private final Person person;
    private final List<Cleaning> cleanings;

    public SalaryReport(
            Person person,
            List<Cleaning> cleanings) {
        this.person = person;
        this.cleanings = cleanings;
    }

    public Person getPerson() {
        return person;
    }

    public List<Cleaning> getCleanings() {
        return cleanings;
    }

    public Integer getAccumulatedSalary() {
        return cleanings.stream()
                .map(c -> c.getCleaningFee())
                .reduce(Integer::sum)
                .orElse(0);
    }
}

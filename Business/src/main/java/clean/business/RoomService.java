package clean.business;

import clean.business.exception.InvalidInputException;
import clean.domain.Room;
import clean.repository.RoomRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class RoomService {

    private final RoomRepository repository;

    @Autowired
    public RoomService(RoomRepository repository) {
        this.repository = repository;
    }

    public List<Room> getAllRooms() {
        return repository.findAll();
    }
    
    public Room createRoom(Room room) {
        if (room.getName() == null || room.getName().isBlank()) {
            throw new InvalidInputException("Rum måste ha läsbara namn");
        }
        return repository.save(room);
    }
}

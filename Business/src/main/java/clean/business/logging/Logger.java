package clean.business.logging;

import java.util.function.Supplier;

/**
 *
 * @author thomas
 */
public interface Logger {

    static final String SELECTED_LOGGER = System.getProperty("cleaning.logger", "JAVA");
    
    void info(Supplier<String> message);

    void error(Throwable ex);

    static Logger get() {
        return SELECTED_LOGGER.equalsIgnoreCase("JAVA")
                ? new JavaApiLogger()
                : new BasicLogger();
    }
}

package clean.business.logging;

import java.util.function.Supplier;

/**
 *
 * @author thomas
 */
public class BasicLogger implements Logger {

    BasicLogger() {
    }

    @Override
    public void info(Supplier<String> message) {
        System.out.println(message.get());
    }

    @Override
    public void error(Throwable ex) {
        ex.printStackTrace(System.out);
    }
}

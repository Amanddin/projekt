package clean.business.logging;

import java.util.function.Supplier;
import java.util.logging.Level;

/**
 *
 * @author thomas
 */
public class JavaApiLogger implements Logger {

    JavaApiLogger() {
    }

    @Override
    public void info(Supplier<String> messageSupplier) {
        java.util.logging.Logger.getLogger(
                JavaApiLogger.class.getName()).log(Level.INFO,
                messageSupplier
        );
    }

    @Override
    public void error(Throwable ex) {
        java.util.logging.Logger.getLogger(JavaApiLogger.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
    }
}

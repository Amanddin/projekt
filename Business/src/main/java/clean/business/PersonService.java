package clean.business;

import clean.business.logging.Logger;
import clean.domain.*;
import clean.repository.PersonRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.*;

/**
 *
 * @author thomas
 */
@Service
public class PersonService {

    private final PersonRepository repository;

    @Autowired
    public PersonService(PersonRepository repository) {
        this.repository = repository;
    }

    public List<Person> getAllPeople() {
        Logger.get().info(() -> "All people service executing");
        return repository.findAll();
    }

    public Person saveNewPerson(Person person, String uuid) {
        if (isAdmin(uuid)) {
            person.createUuid();
            Logger.get().info(() -> "New person created by uuid " + uuid);
            return repository.save(person);
        } else {
            throw new RuntimeException("Must be ADMIN to create person entities");
        }
    }

    public Person getPerson(String uuid) {
        Logger.get().info(() -> "Data fetched for person " + uuid);
        return repository.findByUuid(uuid).get();
    }

    private boolean isAdmin(String uuid) {
        return getPerson(uuid).getRole() == Role.ADMIN;
    }
}

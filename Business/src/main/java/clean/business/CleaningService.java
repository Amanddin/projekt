package clean.business;

import clean.business.domain.SalaryReport;
import clean.business.exception.EntityNotFoundException;
import clean.domain.*;
import clean.repository.*;
import java.time.*;
import java.util.*;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thomas
 */
@Service
public class CleaningService {

    private final CleaningRepository repository;
    private final PersonRepository personRepository;
    private final RoomRepository roomRepository;

    @Autowired
    public CleaningService(CleaningRepository repository, PersonRepository personRepository, RoomRepository roomRepository) {
        this.repository = repository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
    }

    public Cleaning createNewCleaningEvent(Long roomId, String uuid) {
        Cleaning result = new Cleaning();
        result.setRoom(roomRepository.findById(roomId).orElseThrow());
        result.setCleaner(personRepository.findByUuid(uuid).orElseThrow());
        result.setExecutionDateTime(LocalDateTime.now());
        return repository.save(result);
    }

    public List<Cleaning> getAllCleaningsSinceLastSalaryDeadlineByPersonUuid(String uuid) {
        Person person = personRepository.findByUuid(uuid).orElseThrow(() -> new EntityNotFoundException("Person med angivet UUID saknas"));
        return repository.findByCleanerIdAndExecutionDateTimeGreaterThan(
                person.getId(),
                Optional.ofNullable(person.getLastSalaryDeadline())
                        .orElse(LocalDate.now().minusYears(2))
                        .atTime(0, 0));
    }

    public SalaryReport getNextSalaryCumulatedFee(String uuid) {
        Person person = personRepository.findByUuid(uuid).orElseThrow(() -> new EntityNotFoundException("Person med angivet UUID saknas"));
        List<Cleaning> cleanings = getAllCleaningsSinceLastSalaryDeadlineByPersonUuid(uuid);
        return new SalaryReport(person, cleanings);
    }
    
    public List<Cleaning> getCleaningsByRoomId(Long roomId) {
        return repository.findByRoomId(roomId);
    }
}

package clean.rest.dto;

import clean.domain.Person;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@Mapper(componentModel = "spring")
public interface PersonMapper {

    PersonLimitedDto toLimitedDto(Person person);
    
    List<PersonLimitedDto> toLimitedDtoList(List<Person> person);
}

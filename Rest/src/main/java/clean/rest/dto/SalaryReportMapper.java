package clean.rest.dto;

import clean.business.domain.SalaryReport;
import clean.domain.Cleaning;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@Mapper(componentModel = "spring")
public interface SalaryReportMapper {

    @Mapping(target = "name",
            expression = "java(report.getPerson().getFirstName() + \" \" + report.getPerson().getLastName())")
    @Mapping(target = "cleanings", source = "report.cleanings")
    public SalaryReportDto toDto(SalaryReport report);

    @Mapping(target = "room", source = "room.name")
    public AnonymousCleaningDto toCleaningDto(Cleaning cleaning);
}

package clean.rest.dto;

import java.time.LocalDateTime;

/**
 *
 * @author thomas
 */
public record AnonymousCleaningDto(
        LocalDateTime executionDateTime,
        String room) {
}

package clean.rest.dto;

import java.util.List;

/**
 *
 * @author thomas
 */
public record SalaryReportDto(
        String name,
        List<AnonymousCleaningDto> cleanings,
        Integer accumulatedSalary) {
}

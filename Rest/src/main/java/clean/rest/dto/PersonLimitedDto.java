package clean.rest.dto;

import clean.domain.Role;
import java.time.LocalDate;

/**
 *
 * @author thomas
 */
public record PersonLimitedDto(
        Long id,
        String firstName,
        String lastName,
        LocalDate birthDate,
        Role role) {
}

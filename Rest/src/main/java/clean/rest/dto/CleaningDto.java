package clean.rest.dto;

import java.time.LocalDateTime;

/**
 *
 * @author thomas
 */
public record CleaningDto(
        LocalDateTime executionDateTime,
        String name,
        String room) {
}

package clean.rest.dto;

import clean.domain.Cleaning;
import java.util.List;
import org.mapstruct.*;
import org.springframework.stereotype.Component;

/**
 *
 * @author thomas
 */
@Component
@Mapper(componentModel = "spring")
public interface CleaningMapper {

    @Mapping(target = "name",
            expression = "java(cleaning.getCleaner().getFirstName() + \" \" + cleaning.getCleaner().getLastName())")
    @Mapping(target = "room", source = "room.name")
    CleaningDto toDto(Cleaning cleaning);

    List<CleaningDto> toDtoList(List<Cleaning> cleaning);
}

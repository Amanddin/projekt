package clean.rest.exception;

import clean.business.exception.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 *
 * @author thomas
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleMissingEntity(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(
                ex,
                ErrorResponse.create("0x01", ex, HttpStatus.NOT_FOUND),
                HttpHeaders.EMPTY,
                HttpStatus.NOT_FOUND,
                request);
    }

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<Object> handleInvalidInput(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(
                ex,
                ErrorResponse.create("0x02", ex, HttpStatus.BAD_REQUEST),
                HttpHeaders.EMPTY,
                HttpStatus.BAD_REQUEST,
                request);
    }
}

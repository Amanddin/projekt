package clean.rest.exception;

import java.time.LocalDateTime;
import org.springframework.http.HttpStatus;

/**
 *
 * @author thomas
 */
public record ErrorResponse(
        LocalDateTime timestamp,
        String code,
        String message,
        String status) {

    public static ErrorResponse create(
            String code,
            Exception cause,
            HttpStatus status) {
        return new ErrorResponse(
            LocalDateTime.now(),
            code,
            cause.getMessage(),
            status.value() + " (" + status.getReasonPhrase() + ")");
    }
}

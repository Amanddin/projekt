package clean.rest.controller;

import clean.business.PersonService;
import clean.domain.Person;
import clean.rest.dto.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("person")
public class PersonController {

    private final PersonService service;
    private final PersonMapper mapper;

    @Autowired
    public PersonController(PersonService service, PersonMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<PersonLimitedDto> getAllPeople() {
        return mapper.toLimitedDtoList(service.getAllPeople());
    }

    @GetMapping("{uuid}")
    public Person getPerson(@PathVariable String uuid) {
        return service.getPerson(uuid);
    }
    
    @PostMapping
    public Person saveNewPerson(
            @RequestBody Person person,
            @RequestHeader(required = true) String uuid) {
        return service.saveNewPerson(person, uuid);
    }
}

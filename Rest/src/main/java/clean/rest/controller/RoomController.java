package clean.rest.controller;

import clean.business.RoomService;
import clean.domain.Room;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("room")
public class RoomController {

    private final RoomService service;

    @Autowired
    public RoomController(RoomService service) {
        this.service = service;
    }

    @GetMapping
    public List<Room> getAllRooms() {
        return service.getAllRooms();
    }

    @PostMapping
    public Room saveNewRoom(@RequestBody Room room) {
        return service.createRoom(room);
    }
}

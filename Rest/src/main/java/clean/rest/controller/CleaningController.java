package clean.rest.controller;

import clean.business.CleaningService;
import clean.domain.Cleaning;
import clean.rest.dto.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("cleaning")
public class CleaningController {

    private final CleaningService service;
    private final CleaningMapper mapper;
    private final SalaryReportMapper salaryReportMapper;

    @Autowired
    public CleaningController(CleaningService service, CleaningMapper mapper, SalaryReportMapper salaryReportMapper) {
        this.service = service;
        this.mapper = mapper;
        this.salaryReportMapper = salaryReportMapper;
    }

    @PostMapping("{roomId}")
    public Cleaning createNewCleaningEvent(
            @PathVariable Long roomId,
            @RequestHeader String cleaner) {
        return service.createNewCleaningEvent(roomId, cleaner);
    }

    @GetMapping("{roomId}")
    public List<CleaningDto> getCleaningsByRoomId(@PathVariable Long roomId) {
        return mapper.toDtoList(service.getCleaningsByRoomId(roomId));
    }
    
    @GetMapping("person/{uuid}")
    public List<CleaningDto> getUnpaidCleanings(@PathVariable String uuid) {
        return mapper.toDtoList(service.getAllCleaningsSinceLastSalaryDeadlineByPersonUuid(uuid));
    }
    
    @GetMapping("salary/{uuid}")
    public SalaryReportDto getSalaryInfo(@PathVariable String uuid) {
        return salaryReportMapper.toDto(service.getNextSalaryCumulatedFee(uuid));
    }
}

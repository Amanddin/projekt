package clean.rest.controller;

import org.springframework.web.bind.annotation.*;

/**
 *
 * @author thomas
 */
@RestController
@RequestMapping("system")
public class SystemController {

    @GetMapping("isalive")
    public Object checkIfAlive() {
        return new Object() {
            public final Boolean alive = true;
        };
    }
}
